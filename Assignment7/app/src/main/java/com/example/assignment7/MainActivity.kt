package com.example.assignment7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.example.assignment7.databinding.ActivityMainBinding
import android.R.color
import android.content.res.ColorStateList







class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var states = arrayOf(
        intArrayOf(android.R.attr.state_focused),
        intArrayOf(android.R.attr.state_hovered),
        intArrayOf(android.R.attr.state_enabled),
        intArrayOf()
    )

    var colors = intArrayOf(
        R.color.red,
        R.color.main_text,
        R.color.red,
        R.color.btn_color
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.tiEmail.setBoxStrokeColorStateList(ColorStateList(states, colors))
        setContentView(binding.root)
        binding.signIn.setOnClickListener {
            checkEmail()
        }
    }

    private fun checkEmail(): Boolean {
        val email = binding.emailEditText.text.toString()
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.tiEmail.boxStrokeWidth = 0
            return true
        }
        Toast.makeText(applicationContext, getString(R.string.invalid_email), Toast.LENGTH_SHORT).show()
        binding.tiEmail.boxStrokeWidth = 8
        return false
    }
}